package;


import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.events.TouchEvent;
import flash.text.TextField;
import flash.text.TextFormat;
import flash.ui.Multitouch;
import flash.ui.MultitouchInputMode;
import flash.utils.Timer;


class Main extends Sprite {
	
	private var touchList:TextField;
	
	//clear lists after 3sec on inactivity
	private var clearTimer:Timer;
	
	
	public function new () {
		
		super ();
		if (stage != null) 
		{
			init();
		}
		else 
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
	}
	
	private function init(?E:Event):Void 
	{
		//white on black is better for mobile devices!
		this.graphics.clear();
		this.graphics.beginFill(0);
		this.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
		this.graphics.endFill;
		
		if (hasEventListener(Event.ADDED_TO_STAGE))
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		clearTimer = new Timer(3000);
		clearTimer.addEventListener(TimerEvent.TIMER, onClearRequest);
		
		
		
		
		var _textFormat:TextFormat = new TextFormat(null, 18, 0xffffff);
		
		var intro:TextField = new TextField();
		intro.selectable = false;
		intro.multiline = true;
		intro.wordWrap = true;
		intro.defaultTextFormat = _textFormat;
		
		intro.text = "Project to show how touchEvent aren't handled on mobile devices.";
		intro.text += "\n Support Touch ? " + Multitouch.supportsTouchEvents;
		//intro.text += "\nAlso try to tap / down on top of this screen to see how localY is wrong.";
		intro.x = 10;
		intro.y = 10;
		intro.width = stage.stageWidth-10;
		addChild(intro);
		
		
		
		touchList = new TextField();
		touchList.selectable = false;
		touchList.multiline = true;
		touchList.wordWrap = true;
		touchList.defaultTextFormat = _textFormat;
		
		touchList.text = "EVENTS :";
		touchList.x = 10;
		touchList.y = 70;
		touchList.width = stage.stageWidth - 20;
		touchList.height = (stage.stageHeight) - 70;
		addChild(touchList);
		
		
		
		
		Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
		
		stage.addEventListener(MouseEvent.MOUSE_DOWN, onDown);
		stage.addEventListener(TouchEvent.TOUCH_BEGIN, this.touchBegin);   
        stage.addEventListener(TouchEvent.TOUCH_END, this.touchEnd);   
        stage.addEventListener(TouchEvent.TOUCH_MOVE, this.touchMove); 
		stage.addEventListener(TouchEvent.TOUCH_TAP, onTap);
	}
	
	private function onDown(event:MouseEvent):Void
	{
		touchList.text += "\nMouse Down at " + event.localX + "," + event.localY;
		
		clearTimer.reset();
		clearTimer.start();
	}
	
	private function onTap(event:TouchEvent):Void
	{
		touchList.text += "\nTap at " + event.localX + "," + event.localY;
		
		clearTimer.reset();
		clearTimer.start();
	}
	
	private function touchBegin(e:TouchEvent):Void {
        touchList.text += "\nTouch begin " + e.touchPointID;
    }

    private function touchEnd(e:TouchEvent):Void { 
        touchList.text += "\nTouch end " + e.touchPointID;
		
		clearTimer.reset();
		clearTimer.start();
    }

    private function touchMove(e:TouchEvent):Void { 
        touchList.text += "\nTouch move " + e.touchPointID + " at " + e.stageX + ", " + e.stageY ;
    }
	
	private function onClearRequest(event:TimerEvent):Void
	{
		touchList.text = "EVENTS :";
		clearTimer.stop();
	}
	
	
}