# TouchEvent Sample #

On mobile devices, TouchEvent.TOUCH_TAP is never dispatched.

Even if Multitouch.supportsTouchEvents, TouchEvent.TOUCH_TAP is replaced by a MouseEvent.MOUSE_DOWN.

According docs,

* *Multitouch.inputMode = MultitouchInputMode.NONE* should dispatch MouseEvent
* *Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT* should dispatch TouchEvent
* *Multitouch.inputMode = MultitouchInputMode.GESTURE* should dispatch xxxGestureEvent

and *MultitouchInputMode.GESTURE* is default


Other issue, *event.localY* is wrong on some case.
I'm still working on a raw OpenFL test case because it got it on HaxeFlixel, with a scaled sprite.


### Last versions used for test ###

* OpenFL 1.3.0, 1.4.0 & 2.0.0

It *might* be fixed on more recent version

### Useful links ###

* [HaxeFlixel issue][1] report about touchEvent (now closed)
* [HaxeFlixel issue][2] report about event.localY(now closed)
* [OpenFL issue][3] report
* [OpenFL pull][4] show how others touch event are working


  [1]: https://github.com/HaxeFlixel/flixel/issues/1154
  [2]: https://github.com/HaxeFlixel/flixel/issues/1155
  [3]: https://github.com/openfl/openfl/issues/254
  [4]: https://github.com/openfl/openfl-native/pull/200
