package;

import flash.text.TextField;
import flash.text.TextFormat;
import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.text.FlxTextField;
import flixel.ui.FlxButton;
import openfl.Assets;

/**
 * A FlxState which can be used for the game's menu.
 */
class MenuState extends FlxState
{
	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	override public function create():Void
	{
		super.create();
		
		var testSize:Float = 12;
		
		var flxText:FlxText = new FlxText(20, 20, FlxG.stage.stageWidth, "FlxText TEST", Std.int(testSize), true);
		flxText.font = "assets/fonts/amstrad_cpc464.ttf";
		add(flxText); 

		
		var flxTF:FlxTextField = new FlxTextField(20, 50, FlxG.stage.stageWidth, "FlxTextField TEST", Std.int(testSize), true);
		flxTF.font = "assets/fonts/amstrad_cpc464.ttf";
		add(flxTF); 
		
		
		var amst:AmstradText = new AmstradText(20, 80, "AmstradText TEST", 0xffffff, testSize);
		add(amst);
		
		var amstf:FixedAmstradText = new FixedAmstradText(20, 110, "FixedAmstradText TEST", 0xffffff, testSize);
		add(amstf);
		
		
		var _textField:TextField = new TextField();
		_textField.selectable = false;
		_textField.multiline = true;
		_textField.wordWrap = true;
		var _textFormat:TextFormat = new TextFormat(Assets.getFont("assets/fonts/amstrad_cpc464.ttf").fontName, testSize*FlxCamera.defaultZoom, 0xffffff);
		_textField.defaultTextFormat = _textFormat;
		_textField.embedFonts = true;
		
		_textField.text = "TextField TEST (fontsize)";
		_textField.y = 140*FlxCamera.defaultZoom;
		_textField.x = 20*FlxCamera.defaultZoom;
		_textField.width = FlxG.stage.stageWidth;
		
		FlxG.stage.addChild( _textField );

		
		var _textField2:TextField = new TextField();
		_textField2.selectable = false;
		_textField2.multiline = true;
		_textField2.wordWrap = true;
		
		var _textFormat2:TextFormat = new TextFormat(Assets.getFont("assets/fonts/amstrad_cpc464.ttf").fontName, testSize, 0xffffff);
		//var _textFormat2:TextFormat = new TextFormat(Assets.getFont("assets/fonts/nokiafc22.ttf").fontName, testSize*FlxCamera.defaultZoom, 0xffffff);
		_textField2.defaultTextFormat = _textFormat2;
		_textField2.scaleX  = FlxCamera.defaultZoom;
		_textField2.scaleY  = FlxCamera.defaultZoom;
		_textField2.embedFonts = true;
		
		_textField2.text = "TextField TEST (scaled)";
		_textField2.y = 170*FlxCamera.defaultZoom;
		_textField2.x = 20*FlxCamera.defaultZoom;
		_textField2.width = FlxG.stage.stageWidth;
		
		FlxG.stage.addChild( _textField2 );
		
		
	}
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		super.destroy();
	}

	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
		super.update();
	}	
}