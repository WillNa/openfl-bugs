package;

import flash.display.Sprite;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.Lib;
import flash.ui.Keyboard;
import flixel.FlxGame;
import flixel.FlxState;

import flash.text.Font;
import flash.text.TextField;
import flash.text.TextFormat;

// "Amstrad CPC464 Regular"
@:font("assets/fonts/amstrad_cpc464.ttf")
private class FontAmstrad extends Font {}


// "Nokia Cellphone FC Small"
@:font("assets/fonts/nokiafc22.ttf")
private class FontNokia extends Font {}

// "Arial"
@:font("assets/fonts/arial.ttf")
private class FontArial extends Font { }


class Main extends Sprite 
{
	var gameWidth:Int = 320; // Width of the game in pixels (might be less / more in actual pixels depending on your zoom).
	var gameHeight:Int = 240; // Height of the game in pixels (might be less / more in actual pixels depending on your zoom).
	var initialState:Class<FlxState> = MenuState; // The FlxState the game starts with.
	var zoom:Float = -1; // If -1, zoom is automatically calculated to fit the window dimensions.
	var framerate:Int = 60; // How many frames per second the game should run at.
	var skipSplash:Bool = false; // Whether to skip the flixel splash screen that appears in release mode.
	var startFullscreen:Bool = false; // Whether to start the game in fullscreen on desktop targets
	
	// You can pretty much ignore everything from here on - your code should go in your states.
	
	public static function main():Void
	{	
		Lib.current.addChild(new Main());
	}
	
	public function new() 
	{
		super();
		
		if (stage != null) 
		{
			init();
		}
		else 
		{
			addEventListener(Event.ADDED_TO_STAGE, init);
		}
	}
	
	private function init(?E:Event):Void 
	{
		if (hasEventListener(Event.ADDED_TO_STAGE))
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		#if (!flash)
		Font.registerFont(FontAmstrad);
		Font.registerFont(FontNokia);
		#end
		
		
		
		var _textField:TextField = new TextField();
		_textField.selectable = false;
		_textField.multiline = true;
		_textField.wordWrap = true;
		var _textFormat:TextFormat = new TextFormat("Nokia Cellphone FC Small", 14, 0xffffff);// ("Amstrad CPC464 Regular", 10, 0xffffff);
		_textField.defaultTextFormat = _textFormat;
		_textField.embedFonts = true;
		
		_textField.text = "Select scale x2 to see problem\n\n1/ 640x480 (standard) \n2/ 320x240 (scaled x2)";
		#if (flash)
		_textField.text += "\n\n\nWorks perfect of Flash target.\nTry Android or Windows...";
		#end
		_textField.y = 40;
		_textField.x = 40;
		_textField.width = 500;
		_textField.height = 500;
		
		addChild( _textField );				
		
		
		stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
	}
	
	private function onKeyDown(event:KeyboardEvent)
	{
		stage.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
		
		while ( numChildren > 0)
			removeChildAt(0);
		
		switch(event.keyCode)
		{
			case Keyboard.NUMBER_1, Keyboard.NUMPAD_1:
				gameWidth = 640;
				gameHeight = 480;
			case Keyboard.NUMBER_2, Keyboard.NUMPAD_2:
				gameWidth = 320;
				gameHeight = 240;
		}
		setupGame();
	}
	
	private function setupGame():Void
	{
		var stageWidth:Int = Lib.current.stage.stageWidth;
		var stageHeight:Int = Lib.current.stage.stageHeight;

		if (zoom == -1)
		{
			var ratioX:Float = stageWidth / gameWidth;
			var ratioY:Float = stageHeight / gameHeight;
			zoom = Math.min(ratioX, ratioY);
			gameWidth = Math.ceil(stageWidth / zoom);
			gameHeight = Math.ceil(stageHeight / zoom);
		}
		
		trace("Zoom :"+zoom+ " ("+gameWidth+"x"+gameHeight+")");

		addChild(new FlxGame(gameWidth, gameHeight, initialState, zoom, framerate, framerate, skipSplash, startFullscreen));
	}
}