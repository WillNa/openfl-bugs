package;

import flixel.FlxCamera;
import flash.display.Bitmap;
import flash.geom.Point;
import flash.geom.Rectangle;
import flixel.text.FlxTextField;
import flixel.util.FlxPoint;
import openfl.text.TextFormat;

class FixedAmstradText extends FlxTextField{ 

	public function new(X:Float, Y:Float, ?text:String="", ?color:Int=0x0000FF, ?fontSize:Float=10.0 )
	{
		super(X, Y, Std.int(((text.length * fontSize)+4 ) * FlxCamera.defaultZoom), text,  Std.int(fontSize* FlxCamera.defaultZoom));
		
		this.font = "assets/fonts/amstrad_cpc464.ttf";
		this.color = color;
		this.height +=4;
		
		get_textField().scaleX = 1 / FlxCamera.defaultZoom;
		get_textField().scaleY = 1 / FlxCamera.defaultZoom;	
	}	
	
	override private function get_width():Float
	{
		return (super.get_width()/FlxCamera.defaultZoom);
	}
}
