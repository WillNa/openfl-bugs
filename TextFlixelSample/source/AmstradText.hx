package;

import flash.display.Bitmap;
import flash.geom.Point;
import flash.geom.Rectangle;
import flixel.text.FlxTextField;
import flixel.util.FlxPoint;

class AmstradText extends FlxTextField{ 

	public function new(X:Float, Y:Float, ?text:String="", ?color:Int=0x0000FF, ?fontSize:Float=10.0 )
	{
		super(X, Y, Std.int(text.length * fontSize) + 4, text,  Std.int(fontSize));
		
		this.font = "assets/fonts/amstrad_cpc464.ttf";
		this.color = color;
		this.height += 4;
	}	
}
