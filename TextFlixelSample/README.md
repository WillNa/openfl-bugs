# TextFlixel Sample #

On CPP target, some embedded fonts are cropped on top.

While I first thought it was a HaxeFlixel issue, it happens to be OpenFL one:

A problem with scaled textfield.

Workaround avaible (see FixedAmstradText.hx)

### Last version used for test ###

* OpenFL 1.4.0 & OpenFL 2.0.1
It *might* be fixed on more recent version

### Useful links ###

* [HaxeFlixel issue][1] report (now closed)
* [OpenFL issue][2] report, with a limited workaround


  [1]: https://github.com/HaxeFlixel/flixel/issues/1116
  [2]: https://github.com/openfl/openfl-native/issues/224