# What is this repository for? #

You'll find here several projects based on OpenFL and sometimes HaxeFlixel.

The goal of each project is to help OpenFl and/or HaxeFlixel teams to fix a bug.

Be sure to read the README of each project to learn what's the bug is about.



### Bugs awaiting for a fix ###

* TextFlixelSample : some fonts are cropped on top, only on CPP targets (OpenFL)
* TouchEvent : touchEvent are not handled

### Bugs fixed ###

* 

### Not a bug (my mistake !) ###

*